#!/usr/bin/env python3
import random

# Part 1 - tasks from slide 76
print("="*60)
x: int = 0
while x != 20:
    x = random.randrange(10, 30)
    print(x, end=" ")
print()

print("="*60)
print("Numbers from 20 to 0:")
for num in range(20, -1, -1):
    print(num, end=" ")
print()

# Part 2 - tasks from slide 82

list1 = [1]
list2 = [2]
list3 = [3]
list4 = list()
list4.append(list1)
list4.append(list2)
list4.append(list3)
print("="*60)
print(list4)

numbers = list()
for num in range(100, 116):
    numbers.append(num)
print("="*60)
print(numbers)

float_list = [1.23, 2.34, 3.45, 4.56, 5.67]
print("="*60)
for item in float_list:
    print(item, end=" ")
print()

# Part 3 - tasks from slide 86

tuple1 = tuple(range(1, 101))
print("="*60)
print(tuple1)

u, w, x, y, z = (1, 2, 3, 4, 5)
print("{} {} {} {} {}".format(u, w, x, y, z))

# Part 4 - tasks from slide 88

print("="*60)
my_dict = {}
for num in range(5):
    key = input("Insert key #{}: ".format(num+1))
    while key in my_dict:
        print("Key '{}' already exists. Try again.".format(key))
        key = input("Insert key #{}: ".format(num+1))
    value = input("Insert value #{}: ".format(num+1))
    my_dict[key] = value
print(my_dict)

flowers_list = ["rose", "tulip", "forget-me-not", "snowdrop", "poppy"]
animals_list = ["lion", "dog", "cat", "rat", "rabbit"]
furniture_list = ["chair", "sofa", "bed", "wardrobe", "table"]
name_list = ["Dawid", "Damian", "Agnieszka", "Karolina", "Sylwester"]
dish_list = ["pizza", "spaghetti", "cannelloni", "steak", "kebab"]
my_dict2 = {"flowers": flowers_list, "animals": animals_list, "furnitures": furniture_list, "names": name_list,
            "dishes": dish_list}
print("="*60)
print(my_dict2)
